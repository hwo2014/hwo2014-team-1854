from noob_bot import NoobBot
import sys

class SantaMatilda(NoobBot):

	def __init__(self, socket, name, key, connection):
		super(SantaMatilda,self).__init__(socket, name, key, connection)
		self.throttle_value = 1.0
		self.was_stopped = False


	def on_car_positions(self, data):
		self.do_log(data)
		self.do_log('Current throttle: ' + str(self.throttle_value))
		current_angle = data[0]['angle']
		is_tail_left = current_angle < 0
		is_tail_right = current_angle > 0

		if current_angle > 4.0:
			self.__decelerate()
			self.do_log("PRIMEIRO IF")
			self.do_log("Changing throttle_value: " + str(self.throttle_value))
			self.throttle(self.throttle_value)
			return

		if current_angle < -4.0:
			self.__decelerate()
			self.do_log("SEGUNDO IF")
			self.do_log("Changing throttle_value: " + str(self.throttle_value))
			self.throttle(self.throttle_value)
			return

		if current_angle > -1.0 and current_angle < 1.0:
			self.do_log("TERCEIRO IF")
			self.__accelerate()
			self.throttle(self.throttle_value)
			return
		else:
			self.do_log("ELSE IF")
			self.__accelerate()
			return

	def __decelerate(self):
		self.throttle_value = self.throttle_value / 2

		if self.throttle_value < 0.05:
			self.was_stopped = True
			self.throttle_value = 0.05
		self.do_log("Car decelerate to:" + str(self.throttle_value))

	def __accelerate(self):
		self.throttle_value = self.throttle_value + (self.throttle_value * 0.1)
		if self.throttle_value > 1.0:
			self.throttle_value = 1.0
		self.do_log("Car accelerate to:" + str(self.throttle_value))